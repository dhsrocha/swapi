/**
 * @author <a href="mailto:dhsrocha@gmail.com">Diego Rocha</a>
 * @since 1.0.0
 */
module swapi.test {

  requires swapi.base;

  requires org.junit.jupiter.api;

  opens br.com.dhsrocha.swapi.test to org.junit.jupiter.api;
}