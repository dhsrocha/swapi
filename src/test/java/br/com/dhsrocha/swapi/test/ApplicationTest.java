package br.com.dhsrocha.swapi.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Project's integration strategy test cases.
 *
 * @author <a href="mailto:dhsrocha@gmail.com">Diego Rocha</a>
 * @since 1.0.0
 */
final class ApplicationTest {

  @Test
  final void shouldBeTrue() {
    Assertions.assertTrue(Boolean.TRUE);
  }
}
