package br.com.dhsrocha.swapi;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Redirect;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.UnaryOperator;
import org.json.JSONException;
import org.json.JSONObject;
import spark.Request;
import spark.Response;

/**
 * Application's service for fetching external resources.
 *
 * @author <a href="mailto:dhsrocha@gmail.com">Diego Rocha</a>
 * @since 1.0.0
 */
final class Fetcher {

  private static final Logger LOGGER = System.getLogger(Fetcher.class.getSimpleName());

  private static final HttpClient HTTP_CLIENT =
      HttpClient.newBuilder().followRedirects(Redirect.ALWAYS).build();

  private static final String CHAR_ID = "character_id";
  private static final String SPECIES = "species";
  private static final String CHARACTERS = "characters";
  private static final String NAME = "name";

  private Fetcher() {
  }

  static Response request(final Request req, final Response res) {
    res.type("application/json");

    LOGGER.log(Level.INFO, "Building URI for parametrized people API.");
    final var peopleURI = fromUrl("https://swapi.co/api/people/" + req.queryMap(CHAR_ID).value());

    final UnaryOperator<Optional<HttpResponse>> isFound = o -> o.filter(r -> r.statusCode() == 200);

    LOGGER.log(Level.INFO, "Retrieving reference character from API.", res);
    final var charResponse = request(HttpRequest.newBuilder(peopleURI).build(), isFound);

    LOGGER.log(Level.INFO, "Obtaining reference character's species list.");
    final var speciesAsURIs = toJson(charResponse.body()).getJSONArray(SPECIES).toList();

    LOGGER.log(Level.INFO, "Building URI for parametrized film API.");
    final var filmURI = fromUrl("https://swapi.co/api/films/" + req.queryMap(CHAR_ID).value());

    LOGGER.log(Level.INFO, "Retrieving reference film from API.");
    final var filmResponse = request(HttpRequest.newBuilder(filmURI).build(), isFound);

    LOGGER.log(Level.INFO, "Obtaining matched film's character list.");
    final var charsAsURIs = toJson(filmResponse.body()).getJSONArray(CHARACTERS).toList();

    LOGGER.log(Level.INFO, "Obtaining same species characters from films where referred one is.");
    final var charNamesWithSameSpecies = charsAsURIs.stream().map(Object::toString)
        .map(Fetcher::fromUrl)
        .map(uri -> toJson(request(HttpRequest.newBuilder(uri).build(), isFound).body()))
        .filter(jo -> speciesAsURIs.containsAll(jo.getJSONArray(SPECIES).toList()))
        .map(jo -> jo.get(NAME))
        .toArray();

    res.body(Arrays.toString(charNamesWithSameSpecies));
    LOGGER.log(Level.INFO, "Finishing request.");
    return res;
  }

  private static JSONObject toJson(final Object o) {
    return Optional.of(o).map(String::valueOf).map(JSONObject::new)
        .orElseThrow(() -> new JSONException("Invalid JSON content."));
  }

  private static HttpResponse request
      (final HttpRequest req, final UnaryOperator<Optional<HttpResponse>> filter) {
    try {
      return filter.apply(Optional.of(HTTP_CLIENT.send(req, BodyHandlers.ofString())))
          .orElseThrow(() -> new NoSuchElementException("Resource not retrieved."));
    } catch (final InterruptedException | IOException e) {
      Thread.currentThread().interrupt();
      throw new UnsupportedOperationException(e.getCause());
    }
  }

  private static URI fromUrl(final String url) {
    return Optional.of(url).map(URI::create)
        .orElseThrow(() -> new IllegalArgumentException("Malformed URI."));
  }
}
