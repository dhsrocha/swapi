package br.com.dhsrocha.swapi;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Query parameter validation rules for application's requests.
 *
 * @author <a href="mailto:dhsrocha@gmail.com">Diego Rocha</a>
 * @since 1.0.0
 */
enum Validation implements Predicate<String> {

  IS_POSITIVE_INTEGER(s ->
      Stream.of(s)
          .map(Integer::parseInt)
          .allMatch(i -> i > 0)),

  MAXIMUM_OF_3_DIGITS(s ->
      IS_POSITIVE_INTEGER.predicate
          .and(ss -> Stream.of(ss)
              .map(Integer::parseInt)
              .allMatch(i -> Math.log10(i) + 1 <= 3))
          .test(s));

  private static final Logger LOGGER = System.getLogger(Validation.class.getSimpleName());

  private final Predicate<String> predicate;

  Validation(final Predicate<String> predicate) {
    this.predicate = predicate;
  }

  @Override
  public final boolean test(final String s) {
    return this.predicate.test(s);
  }

  static boolean allMatch(final String s) {
    LOGGER.log(Level.INFO, "Validating {0}.", s);
    return Stream.of(Validation.values()).allMatch(pv -> pv.test(s));
  }

  static boolean allMatch(final String s, final String... ss) {
    return Stream.concat(Stream.of(s), Stream.of(ss)).allMatch(Validation::allMatch);
  }
}
