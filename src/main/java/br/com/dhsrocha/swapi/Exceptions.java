package br.com.dhsrocha.swapi;

import java.util.function.Supplier;

/**
 * Application's exception supplier.
 *
 * @author <a href="mailto:dhsrocha@gmail.com">Diego Rocha</a>
 * @since 1.0.0
 */
enum Exceptions {

  EXPIRATION_TIME_NOT_FINISHED(ExpirationTimeNotFinishedException::new);

  private final Supplier<? extends RuntimeException> ex;

  Exceptions(final Supplier<? extends RuntimeException> ex) {
    this.ex = ex;
  }

  final Class<? extends RuntimeException> asClass() {
    return this.ex.get().getClass();
  }

  public final void trigger() {
    throw ex.get();
  }

  private static final class ExpirationTimeNotFinishedException extends RuntimeException {

    @Override
    public final String getMessage() {
      return "Expiration time not finished.";
    }
  }
}
