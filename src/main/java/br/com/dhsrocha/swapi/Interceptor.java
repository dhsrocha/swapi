package br.com.dhsrocha.swapi;

import java.time.Duration;
import java.time.Instant;
import spark.Filter;
import spark.Request;
import spark.Response;

/**
 * Application's request interceptors.
 *
 * @author <a href="mailto:dhsrocha@gmail.com">Diego Rocha</a>
 * @since 1.0.0
 */
enum Interceptor implements Filter {

  // Try to prevent DDoS attacks.
  CANNOT_REQUEST_AGAIN_FOR_10_MILLIS((req, res) -> {
    req.session(Boolean.TRUE);
    if (Duration.between(
        Instant.ofEpochMilli(req.session().lastAccessedTime()),
        Instant.now()).getNano() <= 10000000) {
      Exceptions.EXPIRATION_TIME_NOT_FINISHED.trigger();
    }
  });

  private final Filter filter;

  Interceptor(final Filter filter) {
    this.filter = filter;
  }

  @Override
  public final void handle(final Request req, final Response res) throws Exception {
    filter.handle(req, res);
  }
}
