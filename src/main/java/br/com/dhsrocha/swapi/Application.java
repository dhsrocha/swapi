package br.com.dhsrocha.swapi;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.NoSuchElementException;
import org.json.JSONException;
import org.json.JSONObject;
import spark.Spark;

/**
 * Application's main entry point.
 *
 * @author <a href="mailto:dhsrocha@gmail.com">Diego Rocha</a>
 * @since 1.0.0
 */
final class Application {

  private static final Logger LOGGER = System.getLogger(Application.class.getSimpleName());

  private static final String FILM_ID = "film_id";
  private static final String CHAR_ID = "character_id";

  public static void main(final String[] args) {
    LOGGER.log(Level.INFO, "Starting application.");

    Spark.initExceptionHandler(e -> {
      LOGGER.log(Level.ERROR, e.getMessage(), e);
      System.exit(1);
    });

    handleAs(Exceptions.EXPIRATION_TIME_NOT_FINISHED.asClass(), 429);
    handleAs(NoSuchElementException.class, 404);
    handleAs(IllegalArgumentException.class, 412);
    handleAs(JSONException.class, 422);
    handleAs(UnsupportedOperationException.class, 422);
    LOGGER.log(Level.INFO, "Exceptions mapped.");

    Spark.before("/api/*", Interceptor.CANNOT_REQUEST_AGAIN_FOR_10_MILLIS);

    Spark.get("/api/jdtest", (q, a) -> {

      if (!Validation.allMatch(q.queryParams(CHAR_ID), q.queryParams(FILM_ID))) {
        throw new IllegalArgumentException("Invalid parameters.");
      }

      return Fetcher.request(q, a).body();
    });
  }

  private static void handleAs(final Class<? extends Exception> ex, final int status) {
    Spark.exception(ex, (e, req, res) -> {
      LOGGER.log(Level.ERROR, e.getMessage(), e);
      res.status(status);
      res.body(new JSONObject()
          .put("status", status)
          .put("exception", e.getClass().getCanonicalName())
          .put("message", e.getMessage())
          .put("cause", e.getCause())
          .toString());
    });
  }
}
