/**
 * @author <a href="mailto:dhsrocha@gmail.com">Diego Rocha</a>
 * @since 1.0.0
 */
module swapi.base {

  requires java.base;
  requires java.net.http;

  requires spark.core;
  requires org.json;

  exports br.com.dhsrocha.swapi;
}