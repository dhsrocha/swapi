# Star Wars API

This repository attends to challenge given by *John Deere BR* company.

## Solution

An application logic written in Java language, based on [SparkJava](http://sparkjava.com) web framework, built with [Gradle](https://gradle.org).

### Contents

- `Application.java` is the application's entry-point and contains and is intended to define all mapped routes and dependent logic supported by the web framework;
- `Fetcher.java` contains the retrieving logic from its [original source](http://swapi.co);
- `Interceptor.java` contains filtering logic for the requests. An implementation for preventing DDoS attacks was defined here;
- `Validation.java` contains validation rules for the requests' parameters.

## Requirements

- The project should be run under a Linux distribution, with JDK 11 in place.

## Instructions to run and use

- In root folder, just type `./gradlew run`;
- Do a `GET` request to `http://localhost:4567/api/jdtest`.
